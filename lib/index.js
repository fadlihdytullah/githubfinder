const githubUserRoot = document.getElementById('github-user-root');

const userSummary = document.getElementById('userSummary');

const searchButton = document.getElementById('search');
console.log(searchButton);
searchButton.addEventListener('click', searchUser);

const clientID = '7f3b5015beb9d0322f87';
const clientSecret = '7aa5f0c89be011bc076b21ce48bed29dfa6cd875';

async function showDetailUser(login) {
  const endpoint = `https://api.github.com/users/${login}?client_id=${clientID}&client_secret=${clientSecret}`;

  const res = await fetch(endpoint);
  const user = await res.json();

  if (!userSummary.classList.contains('show')) {
    userSummary.classList.toggle('show');
  }
  while (userSummary.firstChild) userSummary.removeChild(userSummary.firstChild);

  // User Profile
  const userProfile = document.createElement('div');
  userProfile.classList.add('user-profile');
  const userImage = document.createElement('img');
  userImage.setAttribute('src', user.avatar_url);
  const userName = document.createElement('h2');
  userName.innerHTML = `${user.name} &mdash; ${user.login}`;

  userProfile.appendChild(userImage);
  userProfile.appendChild(userName);

  // User Statistic
  const userStatistic = document.createElement('div');
  userStatistic.classList.add('user-statistic');

  const statisticItem = document.createElement('div');
  statisticItem.classList.add('statistic-item');
  const statisticItemBox = document.createElement('div');
  statisticItemBox.classList.add('box');
  const statisticItemBoxLabel = document.createElement('div');
  statisticItemBoxLabel.classList.add('box-label');

  statisticItemBox.innerHTML = user.public_repos;
  statisticItem.appendChild(statisticItemBox);
  statisticItemBoxLabel.innerHTML = 'Public Repos';
  statisticItem.appendChild(statisticItemBoxLabel);

  userStatistic.appendChild(statisticItem);

  const clear = document.createElement('button');
  clear.classList.add('button');
  clear.classList.add('button-clear');
  clear.innerHTML = 'Clear';

  clear.addEventListener('click', () => userSummary.classList.toggle('show'));

  userSummary.appendChild(userProfile);
  userSummary.appendChild(userStatistic);
  userSummary.appendChild(clear);

  const html = `
<div class="user-profile">
  <img src="https://avatars0.githubusercontent.com/u/3?v=4" />
  <h2>Brad Traversy &mdash; bradtraversy</h2>
</div>

<div class="user-statistic">
  <div class="statistic-item">
    <div>12</div>
    <div>Repos</div>
  </div>
  <div class="statistic-item">
    <div>100</div>
    <div>Followers</div>
  </div>
  <div class="statistic-item">
    <div>76</div>
    <div>Following</div>
  </div>
</div>

<div class="user-bio">
  Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corporis
  debitis animi dolor aliquid natus id, non nisi voluptates modi
  veniam.
</div>
`;
}

function renderUser(user) {
  const userItem = document.createElement('div');
  userItem.classList.add('user-item');
  userItem.setAttribute('data-id', user.login);

  userItem.addEventListener('click', e => {
    e.stopPropagation();
    showDetailUser(user.login);
    window.scrollTo(0, 0);
  });

  const userImage = document.createElement('img');
  userImage.setAttribute('src', user.avatar_url);

  const userLogin = document.createElement('h3');
  userLogin.innerHTML = user.login;

  userItem.appendChild(userImage);
  userItem.appendChild(userLogin);

  githubUserRoot.appendChild(userItem);
}

function showAlert() {
  document.getElementById('alert').classList.toggle('alert-show');
}

async function getAllUsers() {
  const endpoint = `https://api.github.com/users?client_id=${clientID}&client_secret=${clientSecret}`;

  const res = await fetch(endpoint);
  const users = await res.json();
  users.forEach(user => renderUser(user));
}

async function getSingleUser(login) {
  const endpoint = `https://api.github.com/search/users?q=${login}&client_id=${clientID}&client_secret=${clientSecret}`;

  const res = await fetch(endpoint);
  const user = await res.json();

  if (user) {
    while (githubUserRoot.firstChild) githubUserRoot.removeChild(githubUserRoot.firstChild);

    // renderUser(user)
    user.items.forEach(user => renderUser(user));
  } else {
    showAlert();
  }
}

function searchUser() {
  const userLogin = document.getElementById('userLogin');

  if (userLogin.value.trim() === '') {
    showAlert();
    return;
  }

  getSingleUser(userLogin.value);

  userLogin.value = '';
}

document.addEventListener('DOMContentLoaded', () => {
  getAllUsers();
});